﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SingleSource_DL.DBModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class SingleSourcePlatformEntities : DbContext
    {
        public SingleSourcePlatformEntities()
            : base("name=SingleSourcePlatformEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tbl_AddressDetails> tbl_AddressDetails { get; set; }
        public virtual DbSet<tbl_Cart> tbl_Cart { get; set; }
        public virtual DbSet<tbl_ProductCategory> tbl_ProductCategory { get; set; }
        public virtual DbSet<tbl_Products> tbl_Products { get; set; }
        public virtual DbSet<tbl_Users> tbl_Users { get; set; }
        public virtual DbSet<tbl_TransactionDetails> tbl_TransactionDetails { get; set; }
    
        public virtual int sp_alterdiagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_alterdiagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_Cart(Nullable<int> customerID, Nullable<int> productID, Nullable<System.DateTime> createdon, Nullable<int> noofProduct, Nullable<System.DateTime> updatedOn)
        {
            var customerIDParameter = customerID.HasValue ?
                new ObjectParameter("CustomerID", customerID) :
                new ObjectParameter("CustomerID", typeof(int));
    
            var productIDParameter = productID.HasValue ?
                new ObjectParameter("ProductID", productID) :
                new ObjectParameter("ProductID", typeof(int));
    
            var createdonParameter = createdon.HasValue ?
                new ObjectParameter("Createdon", createdon) :
                new ObjectParameter("Createdon", typeof(System.DateTime));
    
            var noofProductParameter = noofProduct.HasValue ?
                new ObjectParameter("NoofProduct", noofProduct) :
                new ObjectParameter("NoofProduct", typeof(int));
    
            var updatedOnParameter = updatedOn.HasValue ?
                new ObjectParameter("UpdatedOn", updatedOn) :
                new ObjectParameter("UpdatedOn", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Cart", customerIDParameter, productIDParameter, createdonParameter, noofProductParameter, updatedOnParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> sp_CheckIsValidUser(string userName, string password)
        {
            var userNameParameter = userName != null ?
                new ObjectParameter("userName", userName) :
                new ObjectParameter("userName", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("sp_CheckIsValidUser", userNameParameter, passwordParameter);
        }
    
        public virtual int sp_creatediagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_creatediagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_dropdiagram(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_dropdiagram", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_GetAllAddressDetails_Result> sp_GetAllAddressDetails()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllAddressDetails_Result>("sp_GetAllAddressDetails");
        }
    
        public virtual ObjectResult<sp_GetAllCart_Result> sp_GetAllCart()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllCart_Result>("sp_GetAllCart");
        }
    
        public virtual ObjectResult<sp_GetAllFeedback_Result> sp_GetAllFeedback()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllFeedback_Result>("sp_GetAllFeedback");
        }
    
        public virtual ObjectResult<sp_GetAllProductCategory_Result> sp_GetAllProductCategory()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllProductCategory_Result>("sp_GetAllProductCategory");
        }
    
        public virtual ObjectResult<sp_GetAllProducts_Result> sp_GetAllProducts()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllProducts_Result>("sp_GetAllProducts");
        }
    
        public virtual ObjectResult<sp_GetAllTransaction_Result> sp_GetAllTransaction()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllTransaction_Result>("sp_GetAllTransaction");
        }
    
        public virtual ObjectResult<sp_GetAllUsers_Result> sp_GetAllUsers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_GetAllUsers_Result>("sp_GetAllUsers");
        }
    
        public virtual ObjectResult<sp_helpdiagramdefinition_Result> sp_helpdiagramdefinition(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagramdefinition_Result>("sp_helpdiagramdefinition", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagrams_Result> sp_helpdiagrams(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagrams_Result>("sp_helpdiagrams", diagramnameParameter, owner_idParameter);
        }
    
        public virtual int sp_renamediagram(string diagramname, Nullable<int> owner_id, string new_diagramname)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var new_diagramnameParameter = new_diagramname != null ?
                new ObjectParameter("new_diagramname", new_diagramname) :
                new ObjectParameter("new_diagramname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_renamediagram", diagramnameParameter, owner_idParameter, new_diagramnameParameter);
        }
    
        public virtual int sp_SaveCustomers(string userName, string emailID, string mobileNo, string password, Nullable<System.DateTime> createdon, Nullable<System.DateTime> modifiedon)
        {
            var userNameParameter = userName != null ?
                new ObjectParameter("userName", userName) :
                new ObjectParameter("userName", typeof(string));
    
            var emailIDParameter = emailID != null ?
                new ObjectParameter("emailID", emailID) :
                new ObjectParameter("emailID", typeof(string));
    
            var mobileNoParameter = mobileNo != null ?
                new ObjectParameter("mobileNo", mobileNo) :
                new ObjectParameter("mobileNo", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            var createdonParameter = createdon.HasValue ?
                new ObjectParameter("Createdon", createdon) :
                new ObjectParameter("Createdon", typeof(System.DateTime));
    
            var modifiedonParameter = modifiedon.HasValue ?
                new ObjectParameter("Modifiedon", modifiedon) :
                new ObjectParameter("Modifiedon", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_SaveCustomers", userNameParameter, emailIDParameter, mobileNoParameter, passwordParameter, createdonParameter, modifiedonParameter);
        }
    
        public virtual int sp_SaveCustomerTransaction(string address, Nullable<System.DateTime> transactionDate, Nullable<bool> transactionStatus, Nullable<int> noofProducts, Nullable<int> transactionAmt, Nullable<int> productID, string description, Nullable<System.DateTime> addedOn)
        {
            var addressParameter = address != null ?
                new ObjectParameter("Address", address) :
                new ObjectParameter("Address", typeof(string));
    
            var transactionDateParameter = transactionDate.HasValue ?
                new ObjectParameter("TransactionDate", transactionDate) :
                new ObjectParameter("TransactionDate", typeof(System.DateTime));
    
            var transactionStatusParameter = transactionStatus.HasValue ?
                new ObjectParameter("TransactionStatus", transactionStatus) :
                new ObjectParameter("TransactionStatus", typeof(bool));
    
            var noofProductsParameter = noofProducts.HasValue ?
                new ObjectParameter("NoofProducts", noofProducts) :
                new ObjectParameter("NoofProducts", typeof(int));
    
            var transactionAmtParameter = transactionAmt.HasValue ?
                new ObjectParameter("TransactionAmt", transactionAmt) :
                new ObjectParameter("TransactionAmt", typeof(int));
    
            var productIDParameter = productID.HasValue ?
                new ObjectParameter("ProductID", productID) :
                new ObjectParameter("ProductID", typeof(int));
    
            var descriptionParameter = description != null ?
                new ObjectParameter("Description", description) :
                new ObjectParameter("Description", typeof(string));
    
            var addedOnParameter = addedOn.HasValue ?
                new ObjectParameter("AddedOn", addedOn) :
                new ObjectParameter("AddedOn", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_SaveCustomerTransaction", addressParameter, transactionDateParameter, transactionStatusParameter, noofProductsParameter, transactionAmtParameter, productIDParameter, descriptionParameter, addedOnParameter);
        }
    
        public virtual int sp_SaveFeedback(Nullable<bool> customerID, Nullable<bool> feedbackID, Nullable<bool> productID, Nullable<double> ratings, string comments, Nullable<int> feedbackDate)
        {
            var customerIDParameter = customerID.HasValue ?
                new ObjectParameter("CustomerID", customerID) :
                new ObjectParameter("CustomerID", typeof(bool));
    
            var feedbackIDParameter = feedbackID.HasValue ?
                new ObjectParameter("FeedbackID", feedbackID) :
                new ObjectParameter("FeedbackID", typeof(bool));
    
            var productIDParameter = productID.HasValue ?
                new ObjectParameter("ProductID", productID) :
                new ObjectParameter("ProductID", typeof(bool));
    
            var ratingsParameter = ratings.HasValue ?
                new ObjectParameter("Ratings", ratings) :
                new ObjectParameter("Ratings", typeof(double));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var feedbackDateParameter = feedbackDate.HasValue ?
                new ObjectParameter("FeedbackDate", feedbackDate) :
                new ObjectParameter("FeedbackDate", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_SaveFeedback", customerIDParameter, feedbackIDParameter, productIDParameter, ratingsParameter, commentsParameter, feedbackDateParameter);
        }
    
        public virtual int sp_SaveProducts(string productName, Nullable<int> productCategory, string productDescription, Nullable<int> unitPrice, Nullable<double> rating, Nullable<int> noofProduct, string imageURL)
        {
            var productNameParameter = productName != null ?
                new ObjectParameter("ProductName", productName) :
                new ObjectParameter("ProductName", typeof(string));
    
            var productCategoryParameter = productCategory.HasValue ?
                new ObjectParameter("ProductCategory", productCategory) :
                new ObjectParameter("ProductCategory", typeof(int));
    
            var productDescriptionParameter = productDescription != null ?
                new ObjectParameter("ProductDescription", productDescription) :
                new ObjectParameter("ProductDescription", typeof(string));
    
            var unitPriceParameter = unitPrice.HasValue ?
                new ObjectParameter("UnitPrice", unitPrice) :
                new ObjectParameter("UnitPrice", typeof(int));
    
            var ratingParameter = rating.HasValue ?
                new ObjectParameter("Rating", rating) :
                new ObjectParameter("Rating", typeof(double));
    
            var noofProductParameter = noofProduct.HasValue ?
                new ObjectParameter("NoofProduct", noofProduct) :
                new ObjectParameter("NoofProduct", typeof(int));
    
            var imageURLParameter = imageURL != null ?
                new ObjectParameter("ImageURL", imageURL) :
                new ObjectParameter("ImageURL", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_SaveProducts", productNameParameter, productCategoryParameter, productDescriptionParameter, unitPriceParameter, ratingParameter, noofProductParameter, imageURLParameter);
        }
    
        public virtual int sp_UpdateCustomers(Nullable<int> id, string userName, string emailID, string mobileNo, string password, Nullable<bool> isEnabled, Nullable<System.DateTime> updatedOn)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("Id", id) :
                new ObjectParameter("Id", typeof(int));
    
            var userNameParameter = userName != null ?
                new ObjectParameter("userName", userName) :
                new ObjectParameter("userName", typeof(string));
    
            var emailIDParameter = emailID != null ?
                new ObjectParameter("emailID", emailID) :
                new ObjectParameter("emailID", typeof(string));
    
            var mobileNoParameter = mobileNo != null ?
                new ObjectParameter("mobileNo", mobileNo) :
                new ObjectParameter("mobileNo", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("password", password) :
                new ObjectParameter("password", typeof(string));
    
            var isEnabledParameter = isEnabled.HasValue ?
                new ObjectParameter("IsEnabled", isEnabled) :
                new ObjectParameter("IsEnabled", typeof(bool));
    
            var updatedOnParameter = updatedOn.HasValue ?
                new ObjectParameter("UpdatedOn", updatedOn) :
                new ObjectParameter("UpdatedOn", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_UpdateCustomers", idParameter, userNameParameter, emailIDParameter, mobileNoParameter, passwordParameter, isEnabledParameter, updatedOnParameter);
        }
    
        public virtual int sp_UpdateProducts(Nullable<int> iD, string productName, Nullable<int> productCategory, string productDescription, Nullable<int> unitPrice, Nullable<double> rating, Nullable<int> noofProduct, string imageURL)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var productNameParameter = productName != null ?
                new ObjectParameter("ProductName", productName) :
                new ObjectParameter("ProductName", typeof(string));
    
            var productCategoryParameter = productCategory.HasValue ?
                new ObjectParameter("ProductCategory", productCategory) :
                new ObjectParameter("ProductCategory", typeof(int));
    
            var productDescriptionParameter = productDescription != null ?
                new ObjectParameter("ProductDescription", productDescription) :
                new ObjectParameter("ProductDescription", typeof(string));
    
            var unitPriceParameter = unitPrice.HasValue ?
                new ObjectParameter("UnitPrice", unitPrice) :
                new ObjectParameter("UnitPrice", typeof(int));
    
            var ratingParameter = rating.HasValue ?
                new ObjectParameter("Rating", rating) :
                new ObjectParameter("Rating", typeof(double));
    
            var noofProductParameter = noofProduct.HasValue ?
                new ObjectParameter("NoofProduct", noofProduct) :
                new ObjectParameter("NoofProduct", typeof(int));
    
            var imageURLParameter = imageURL != null ?
                new ObjectParameter("ImageURL", imageURL) :
                new ObjectParameter("ImageURL", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_UpdateProducts", iDParameter, productNameParameter, productCategoryParameter, productDescriptionParameter, unitPriceParameter, ratingParameter, noofProductParameter, imageURLParameter);
        }
    
        public virtual int sp_upgraddiagrams()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_upgraddiagrams");
        }
    }
}
