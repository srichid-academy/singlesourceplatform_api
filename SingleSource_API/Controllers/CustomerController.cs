﻿using SingleSource_BL.Concrete;
using SingleSource_BL.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SingleSource_API.Controllers
{
    public class CustomerController : ApiController
    {
        private Customer objCustomer = new Customer();

        // GET: api/Customer

        public IEnumerable<CustomerViewModel> GetCustomers()
        {
            return objCustomer.GetAllCustomers();
        }

        //public IEnumerable<CustomerViewModel> Login()
        //{
        //    return objCustomer.Login();
        //}
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Customer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Customer
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Customer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Customer/5
        public void Delete(int id)
        {
        }
    }
}
