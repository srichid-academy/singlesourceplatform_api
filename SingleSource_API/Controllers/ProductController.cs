﻿using SingleSource_BL.Concrete;
using SingleSource_BL.Dto;
using SingleSource_DL.DBModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SingleSource_API.Controllers
{
    public class ProductController : ApiController
    {
        private Product objProduct = new Product();


       

        // GET: api/Product
        public IEnumerable<ProductViewModel> GetProducts()
        {
            //var data = new ArrayList();
            //data.Add(1);
            //data.Add("Gangireddy");

            return objProduct.GetAllProducts();
        }
        public IEnumerable<ProductViewModel> GetProductsByCategory(int id)
        {
            return objProduct.GetProductsByCategory(id);
        }

        public bool PostSaveProducts(ProductViewModel product)
        {
            return objProduct.SaveProduct(product);
        }

        public bool PostSaveCategory(ProductViewModel product)
        {
            return objProduct.SaveProduct(product);
        }

        public bool PostUpdateProducts(ProductViewModel product)
        {
            return objProduct.UpdateProduct(product);
        }
        // GET: api/Product/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Product
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Product/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Product/5
        public void Delete(int id)
        {
        }
    }
}
