﻿using SingleSource_BL.Concrete;
using SingleSource_BL.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SingleSource_API.Controllers
{
    public class CartController : ApiController
    {
        private Cart objCart = new Cart();
        // GET: api/Cart
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Cart/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Cart

        public bool PostSaveCart(CartViewModel cart)
        {
            return objCart.SaveCart(cart);
        }
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Cart/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cart/5
        public void Delete(int id)
        {
        }
    }
}
