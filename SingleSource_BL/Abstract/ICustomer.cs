﻿using SingleSource_BL.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Abstract
{
    interface ICustomer
    {
        List<ProductViewModel> GetAllCustomer();
        List<ProductViewModel> Login(string EmailID, string Passwo );
    }
}
