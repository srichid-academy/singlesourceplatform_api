﻿using SingleSource_BL.Dto;
using SingleSource_DL.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Abstract
{
    interface IProduct
    {
        List<ProductViewModel> GetAllProducts();
        List<ProductViewModel> GetProductsByCategory(int categoryId);
        bool SaveProduct(ProductViewModel producct);
    }
}
