﻿using SingleSource_BL.Dto;
using SingleSource_DL.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Concrete
{
    public class Cart
    {
        private SingleSourcePlatformEntities dbConnection = new SingleSourcePlatformEntities();
        public bool SaveCart(CartViewModel cart)
        {
            var newCart = new tbl_Cart();
            newCart.ID = Convert.ToInt32(cart.CartID);
            newCart.CustomerID = cart.CustomerId;
            newCart.ProductID = Convert.ToInt32(cart.ProductID);
            

            dbConnection.tbl_Cart.Add(newCart);
            dbConnection.SaveChanges();

            return true;
        }
    }
}
