﻿using SingleSource_BL.Dto;
using SingleSource_DL.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Concrete
{

    public class Customer
    {
        private SingleSourcePlatformEntities dbConnection = new SingleSourcePlatformEntities();
        public List<CustomerViewModel> GetAllCustomers()
        {
            var CustomerList = dbConnection.tbl_Users.Select(c => new CustomerViewModel { CustomerId = c.UserId, CustomerName = c.FullName, EmailID = c.EmailID, MobileNo = c.MobileNo });

            return CustomerList.ToList();
        }

        public List<CustomerViewModel> GetCustomersDetails()
        {
            //var FullCustomerList = dbConnection.tbl_Users.Select(c => new CustomerViewModel { Address = c.Address, State = c.State, City = c.City, Country = c.Country }).Select(c => new CustomerViewModel { CustomerId = c.UserId, CustomerName = c.FullName, EmailID = c.EmailID, MobileNo = c.MobileNo }));

            return null;
        }


    }
}
