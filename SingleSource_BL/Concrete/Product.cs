﻿using SingleSource_BL.Abstract;
using SingleSource_BL.Dto;
using SingleSource_DL.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Concrete
{
    public class Product
    {
        private SingleSourcePlatformEntities dbConnection =new SingleSourcePlatformEntities();

        public List<ProductViewModel> GetAllProducts()
        {
            var productList = dbConnection.tbl_Products.Select(p => new ProductViewModel { ProductId = p.ProductID, ProductName = p.ProductName, Description = p.ProductDescription, NoofProducts = p.NoofProduct.ToString(), UnitPrice = p.UnitPrice.ToString(), Rating = p.Rating.ToString(), Category = p.ProductCategory.ToString() });

            return productList.ToList();
        }
        public List<ProductViewModel> GetProductsByCategory(int categoryId)
        {
            var category = dbConnection.tbl_Products.Where(p=> p.ProductCategory == categoryId).Select(p => new ProductViewModel { ProductId = p.ProductID, ProductName = p.ProductName, Description = p.ProductDescription, NoofProducts = p.NoofProduct.ToString(), UnitPrice = p.UnitPrice.ToString(), Rating = p.Rating.ToString(), Category = p.ProductCategory.ToString() });

            return category.ToList();
        }

        public bool SaveProduct(ProductViewModel product)
        {
            var newproduct = new tbl_Products();
            newproduct.ProductName = product.ProductName;
            newproduct.ProductDescription = "";
            newproduct.ImageURL = "";
            newproduct.ProductCategory = Convert.ToInt32(product.Category);
            newproduct.Rating = 3;
            newproduct.NoofProduct = Convert.ToInt32(product.NoofProducts);

            dbConnection.tbl_Products.Add(newproduct);
            dbConnection.SaveChanges();

            return true;
        }

        public bool SaveCategory(ProductViewModel category)
        {
            var newCategory = new tbl_ProductCategory();
            newCategory.CategoryName = category.ProductName;
            newCategory.ProductCategoryID = Convert.ToInt32(category.CategoryName);
          
            dbConnection.tbl_ProductCategory.Add(newCategory);
            dbConnection.SaveChanges();

            return true;
        }

        public bool SaveCart(ProductViewModel product)
        {
            var newproduct = new tbl_Products();
            newproduct.ProductName = product.ProductName;
            newproduct.ProductDescription = "";
            newproduct.ImageURL = "";
            newproduct.ProductCategory = Convert.ToInt32(product.Category);
            newproduct.Rating = 3;
            newproduct.NoofProduct = Convert.ToInt32(product.NoofProducts);

            dbConnection.tbl_Products.Add(newproduct);
            dbConnection.SaveChanges();

            return true;
        }
        public bool UpdateProduct(ProductViewModel product)
        {
            //var newproduct = new tbl_Products();
            //newproduct.ProductName = product.ProductName;
            //newproduct.ProductDescription = "";
            //newproduct.ImageURL = "";
            //newproduct.ProductCategory = Convert.ToInt32(product.Category);
            //newproduct.Rating = 3;
            //newproduct.NoofProduct = Convert.ToInt32(product.StockAvailable);

            var updateProduct = dbConnection.tbl_Products.Where(p => p.ProductID == product.ProductId).SingleOrDefault();
            updateProduct.NoofProduct = Convert.ToInt32(product.NoofProducts);

            dbConnection.tbl_Products.Add(updateProduct);
            dbConnection.Entry(updateProduct).State = System.Data.Entity.EntityState.Modified;
            dbConnection.SaveChanges();

            return true;


        }


    }
}
