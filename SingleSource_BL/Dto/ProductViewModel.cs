﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Dto
{
    public class ProductViewModel
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public string Category { get; set; }

        public string Description { get; set; }
        public string UnitPrice { get; set; }

        public string Rating { get; set; }

        public string NoofProducts { get; set; }

        public string ImageURl { get; set; }
        public string CategoryName { get; set; }

    }

}
