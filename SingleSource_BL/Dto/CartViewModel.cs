﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleSource_BL.Dto
{
    public class CartViewModel
    {
        public int CustomerId { get; set; }
        public string CartID { get; set; }
        public string ProductID { get; set; }
        
    }
}
